from setuptools import setup

setup(name='input_utils',
      version='0.1',
      description='User input utils to make your programs shorter and secure',
      url='https://gitlab.com/miskopo/input_utils',
      author='Michal Polovka',
      author_email='michal.polovka@gmail.com',
      license='GNU/GPLv3',
      packages=['input_utils'],
      zip_safe=False,
      description='Simplify user inputs in Python',
      classifiers=[
    # How mature is this project? Common values are
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 3 - Alpha',

    # Indicate who your project is intended for
    'Intended Audience :: Developers',
    'Topic :: Software Development :: User Input Tools',

    # Pick your license as you wish (should match "license" above)
     'License :: OSI Approved :: GNU/GPLv3 License',

    # Specify the Python versions you support here. In particular, ensure
    # that you indicate whether you support Python 2, Python 3 or both.
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.2',
    'Programming Language :: Python :: 3.3',
    'Programming Language :: Python :: 3.4',
],
      keywords='input')
