## Python input utils ##
[![pipeline status](https://gitlab.com/miskopo/input_utils/badges/master/pipeline.svg)](https://gitlab.com/miskopo/input_utils/commits/master)
[![coverage report](https://gitlab.com/miskopo/input_utils/badges/master/coverage.svg)](https://gitlab.com/miskopo/input_utils/commits/master)
[![PyPI](https://img.shields.io/pypi/v/input_utils.svg?style=flat-square)](https://pypi.python.org/pypi/input_utils)
[![PyPI](https://img.shields.io/pypi/wheel/input_utils.svg?style=flat-square)](https://pypi.python.org/pypi/input_utils)
[![Documentation Status](https://readthedocs.org/projects/input-utils/badge/?version=latest)](http://input-utils.readthedocs.io/en/latest/?badge=latest)
[![GNU General Public License](https://img.shields.io/badge/license-GPL%20v3-orange.svg?style=flat-square)](http://www.gnu.org/licenses/gpl-3.0.en.html)


Have you ever needed to make simple yes/no dialog in Python? Or interactive action prompt with predefined choices?
You found the right package!

This module helps you with these and other tedious tasks!

## :warning: Under developement  :warning: ##

### Installation ###

If you have `pip` installed, simply type

``pip install input_utils``

or

``python -m pip install input_utils``

If you don't have `pip` installed, click on the penguin (duck was busy):   [:penguin:](http://lmgtfy.com/?s=d&q=how+to+install+python+pip)

### Usage ###
_Not yet applicable_
