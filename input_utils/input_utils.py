def input_yes_no(prompt, default='yes',
                 invalid_message="Invalid choice, try again") -> str:
    """
    Function simplifies user binary (yes/no) input.
    :param prompt: Prompt to be used
    :param default: Default choice
    :param invalid_message: Message to be printed in case of invalid input
    :return: string yes or no
    """
    while True:
        choice = input("{} [{}]".format(prompt, default) or default)
        if choice in ('yes', 'y'):
            return 'yes'
        elif choice in ('no', 'n'):
            return 'no'
        else:
            print(invalid_message)


def input_yes_no_bool(prompt, default='yes',
                      invalid_message="Invalid choice, try again") -> bool:
    """
    :param prompt: Prompt to be used
    :param default: Default choice
    :param invalid_message: Message to be printed in case of invalid input
    :return: True if user chose yes, False if no
    """
    return input_yes_no(prompt, default, invalid_message) == 'yes'


def input_with_choices(prompt, choices, default, invalid_message,
                       print_choices_list=True) -> str:
    """
    :param prompt: Prompt to be used
    :param choices: List of possible choices
    :param default: Default choice
    """
    while True:
        if print_choices_list:
            for index, choice in enumerate(choices):
                print("{}: {}".format(index + 1, choice))
        choice = input("{} [{}]".format(prompt, default) or default)
        if choice.lower() in map(str.lower, choices):
            return choice
        else:
            print(invalid_message)
